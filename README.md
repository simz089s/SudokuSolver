# sudoku_solver
A sudoku solving program written in C using naive backtracking. Reads from text file and prints solution to stdout. Mainly for 3x3 but works with other sizes.
